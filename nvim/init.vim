""""""""""""""""""""""""""""""""""""""""""""""""""
"                   GENERAL                      "
""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible            " Disable compatibility to old-time vi
set showmatch               " Show matching brackets.
set ignorecase              " Do case insensitive matching
set hlsearch                " highlight search results
set nu rnu                  " add line numbers
set wildmode=longest,list   " get bash-like tab completions
"Enable mouse click for nvim
set mouse=a
"Fix cursor replacement after closing nvim
set guicursor=


""""""""""""""""""""""""""""""""""""""""""""""""""
"                   WHITE SPACE                  "
""""""""""""""""""""""""""""""""""""""""""""""""""
set tabstop=2               " number of columns occupied by a tab character
set softtabstop=2           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=2            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed

set list listchars=tab:>\ ,trail:+,eol:$ " See invisible characters

inoremap <S-Tab> <C-d>


""""""""""""""""""""""""""""""""""""""""""""""""""
"                   PLUGINS                      "
""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin(stdpath('data') . '/plugged')
" vim-surround
Plug 'tpope/vim-surround'

" gitgutter
Plug 'airblade/vim-gitgutter'

" Readline style insertion
Plug 'tpope/vim-rsi'

" vimwiki
Plug 'vimwiki/vimwiki'

" Color schemes
Plug 'christianchiarulli/nvcode-color-schemes.vim'

Plug 'sainnhe/sonokai'

call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""
"               PLUGINS SETTINGS                 "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Disable Indent Lines for vimwiki
" autocmd FileType vimwiki : IndentLinesDisable
autocmd FileType wiki : indentLineDisable

""""""""""""""""""""""""""""""""""""""""""""""""""
"                   COLORS                       "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable Syntax Highlighting
syntax on

" checks if your terminal has 24-bit color support
if (has("termguicolors"))
  set termguicolors
  hi LineNr ctermbg=NONE guibg=NONE
endif

" Set colorscheme and amount of colors
let g:nvcode_termcolors=256
colorscheme sonokai

" Set 80 char column border
set cc=80
hi ColorColumn ctermbg=11

""""""""""""""""""""""""""""""""""""""""""""""""""
"                   MOVEMENTS                    "
""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
